﻿using TransactionDemo.SeedWorks.Constants;

namespace TransactionDemo.Core.BusinessModels.Transaction
{
    public class TransactionRequest
    {
        public int SenderBankID { get; set; }
        public int? ReceiverBankID { get; set; } 
        public decimal Amount { get; set; }
        public string FromAccountNumber { get; set; } = null!;
        public string? ToAccountNumber { get; set; }
    }
}
