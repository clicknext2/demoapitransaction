﻿using Microsoft.EntityFrameworkCore;
using TransactionDemo.Core.BusinessModels.Transaction;
using TransactionDemo.Core.Repositories.Account;
using TransactionDemo.Core.Repositories.Bank;
using TransactionDemo.Core.Repositories.User;
using TransactionDemo.Core.RepositoryModels;
using TransactionDemo.SeedWorks;
using TransactionDemo.SeedWorks.Constants;

namespace TransactionDemo.Core.BusinessLogics.Transaction
{
    public class TransactionOverview
    {
        readonly ITransactionRepository transactionRepository;
        readonly IAccountRepository accountRepository;
        readonly IBankRepository bankRepository;

        public TransactionOverview(ITransactionRepository transactionRepository
            , IAccountRepository accountRepository
            , IBankRepository bankRepository
            )
        {
            this.transactionRepository = transactionRepository;
            this.accountRepository = accountRepository;
            this.bankRepository = bankRepository;
        }

        public async Task<ResultApi<List<TransactionOverviewResponse>>> GetTransactions(TransactionOverviewRequest request)
        {

            try
            {
                var allAccount = await (from a in accountRepository.Queryable
                                        select a).ToListAsync();

                var accountResult = (from a in allAccount
                                     where a.UserId == request.UserID
                                     join b in bankRepository.Queryable on a.BankId equals b.BankId
                                     select new TransactionOverviewResponse
                                     {
                                         AccountID = a.AccountId,
                                         AccountName = a.Name,
                                         BankID = b.BankId, 
                                         BankName = b.Name,
                                         AccountNumber = a.AccountNumber,
                                         Remain = a.Balance,
                                         Transactions = new List<TransactionTransferResponse>(),
                                     }).ToList();

                var listAccount = accountResult.Select(s => s.AccountID).ToList();
                var accountTransactionLookup = accountResult.ToDictionary(x => x.AccountID, x => x.Transactions);
                var accountNameLookup = allAccount.ToDictionary(x => x.AccountId, x => x.Name);

                var listStatusTransaction = new List<TransactionTypeID>
                {
                    TransactionTypeID.Deposit,
                    TransactionTypeID.Withdraw,
                };

                var transactionTransferResult = await (from a in transactionRepository.Queryable
                                                       where listAccount.Contains(a.FromAccountId) && a.Type == TransactionTypeID.Transfer
                                                       orderby a.CreatedAt descending 
                                                       select a).ToListAsync();

                var transactionRecieveResult = await (from a in transactionRepository.Queryable
                                                      where listAccount.Contains(a.ToAccountId!.Value) && a.Type == TransactionTypeID.Recieve
                                                      orderby a.CreatedAt descending 
                                                      select a).ToListAsync();

                var transactionDepositOrWithdrawResult = await (from a in transactionRepository.Queryable
                                                                where listAccount.Contains(a.FromAccountId) && listStatusTransaction.Contains(a.Type)
                                                                orderby a.CreatedAt descending
                                                                select a).ToListAsync();

                foreach (var transaction in transactionTransferResult)
                {
                    var list = accountTransactionLookup[transaction.FromAccountId];
                    list!.Add(new TransactionTransferResponse
                    {
                        TransactionID = transaction.TransactionId,
                        Amount = transaction.Amount,
                        Date = transaction.CreatedAt,
                        ToAccountName = accountNameLookup[transaction.ToAccountId!.Value],
                        FromAccountName = accountNameLookup[transaction.FromAccountId],
                        TransactionType = transaction.Type,
                    });
                }

                foreach (var transaction in transactionRecieveResult)
                {
                    var list = accountTransactionLookup[transaction.ToAccountId!.Value];
                    list!.Add(new TransactionTransferResponse
                    {
                        TransactionID = transaction.TransactionId,
                        Amount = transaction.Amount,
                        Date = transaction.CreatedAt,
                        FromAccountName = accountNameLookup[transaction.FromAccountId],
                        TransactionType = transaction.Type,
                    });
                }

                foreach (var transaction in transactionDepositOrWithdrawResult)
                {
                    var list = accountTransactionLookup[transaction.FromAccountId];
                    list!.Add(new TransactionTransferResponse
                    {
                        TransactionID = transaction.TransactionId,
                        Amount = transaction.Amount,
                        Date = transaction.CreatedAt,
                        TransactionType = transaction.Type,
                    });
                }
                return new ResultApi<List<TransactionOverviewResponse>> { Message = "Success", StatusCode = 200, Data = accountResult };
            }
            catch (Exception ex)
            {
                return new ResultApi<List<TransactionOverviewResponse>> { Message = ex.Message, StatusCode = 500 };
            }

        }
    }
}
