﻿using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using TransactionDemo.Core.Repositories.User;

namespace TransactionDemo.Core.BusinessLogics.Authentication
{
    public class AuthenticationManage
    {
        readonly IUserRepository userRepository;
        private readonly IConfiguration configuration;

        public AuthenticationManage(IUserRepository userRepository, IConfiguration configuration)
        {
            this.userRepository = userRepository;
            this.configuration = configuration;
        }

        public async Task<(string? token, Guid? userId)> Authentication(string userName, string password)
        {
            try
            {
                var user = await (from a in userRepository.Queryable
                                  where a.Username == userName
                                     && a.Password == password
                                  select a).SingleOrDefaultAsync();

                if (user == null)
                    return (null, null);

                var userId = user.UserId; 

                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(configuration["Jwt:Key"]);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new[]
                    {
                        new Claim(ClaimTypes.Name, userName),
                        new Claim("userId", userId.ToString()) 
                    }),
                    Expires = DateTime.UtcNow.AddMinutes(10),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };

                var token = tokenHandler.CreateToken(tokenDescriptor);
                return (tokenHandler.WriteToken(token), userId);
            }
            catch
            {
                return (null, null);
            }
        }

    }
}
