﻿using Microsoft.EntityFrameworkCore;
using TransactionDemo.Core.BusinessModels.Bank;
using TransactionDemo.Core.BusinessModels.Transaction;
using TransactionDemo.Core.Repositories.Bank;
using TransactionDemo.SeedWorks;

namespace TransactionDemo.Core.BusinessLogics.Bank
{
    public class BankOverview
    {
        readonly IBankRepository bankRepository;

        public BankOverview(IBankRepository bankRepository)
        {
            this.bankRepository = bankRepository;
        }

        public async Task<ResultApi<List<BankOverviewResponse>>> GetBanks()
        {
            var result = new List<BankOverviewResponse>();
            try
            {
                result = await (from bank in bankRepository.Queryable
                                select new BankOverviewResponse
                                {
                                 BankID = bank.BankId,
                                 Name = bank.Name,
                                }).ToListAsync();

                return new ResultApi<List<BankOverviewResponse>> { Message = "Success", StatusCode = 200, Data = result };
            }
            catch (Exception ex)
            {
                return new ResultApi<List<BankOverviewResponse>> { Message = ex.Message, StatusCode = 500 };
            }
        }
    }
}
