﻿using TransactionDemo.SeedWorks.Constants;

namespace TransactionDemo.Core.RepositoryModels
{
    public class TransactionModel
    {
        public Guid TransactionId { get; set; }
        public TransactionTypeID Type { get; set; }
        public decimal Amount { get; set; }
        public Guid FromAccountId { get; set; }
        public Guid? ToAccountId { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
