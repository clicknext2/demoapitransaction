﻿using TransactionDemo.Core.RepositoryModels;

namespace TransactionDemo.Core.Repositories.Bank
{
    public interface IBankRepository
    {
        IQueryable<BankModel> Queryable { get; }
    }
}
