﻿using Microsoft.EntityFrameworkCore;
using TransactionDemo.Core.Repositories.User;
using TransactionDemo.Core.RepositoryModels;
using TransactionDemo.Database.Models;
using TransactionDemo.SeedWorks.Constants;

namespace TransactionDemo.Core.Repositories.Transaction
{
    public class TransactionRepository : ITransactionRepository
    {
        readonly TransactionDemoContext _context;

        public TransactionRepository(TransactionDemoContext context)
        {
            _context = context;
        }

        public IQueryable<TransactionModel> Queryable => _context.Transactions.Select(x => new TransactionModel
        {
            TransactionId = x.TransactionId,
            Amount = x.Amount,
            CreatedAt = x.CreatedAt,
            FromAccountId = x.FromAccountId,
            ToAccountId = x.ToAccountId,
            Type = (TransactionTypeID)x.Type
        });

        public async Task Insert(TransactionModel model)
        {
            await _context.Transactions.AddAsync(new TransactionDemo.Database.Models.Transaction
            {
                TransactionId = model.TransactionId,  
                Amount = model.Amount,
                Type = (int)model.Type,
                ToAccountId = model.ToAccountId,
                FromAccountId= model.FromAccountId,
                CreatedAt= model.CreatedAt,
            });
        }
    }
}
