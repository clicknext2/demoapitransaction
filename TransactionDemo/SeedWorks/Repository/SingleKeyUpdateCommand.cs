﻿namespace TransactionDemo.SeedWorks.Repository
{
    public class SingleKeyUpdateCommand<T> : UpdateCommand<T>
    {
        public Guid Id { get; set; }
    }
}
