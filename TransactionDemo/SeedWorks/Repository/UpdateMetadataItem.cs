﻿namespace TransactionDemo.SeedWorks.Repository
{
    public class UpdateMetadataItem<TEntityModel>
    {
        public string RepositoryModelPropertyName { get; set; }
        public string EntityModelPropertyName { get; set; }
        public Func<object, object> Converter { get; set; }
    }
}
