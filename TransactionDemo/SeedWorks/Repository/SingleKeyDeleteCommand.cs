﻿namespace TransactionDemo.SeedWorks.Repository
{
    public class SingleKeyDeleteCommand
    {
        public Guid Id { get; set; }
    }
}
